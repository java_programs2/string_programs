package string_programs;

import java.util.Arrays;

public class Occurance {

	public static void main(String[] args) {
		String name="aabdabeecbbd";
		char ch[]=name.toCharArray();
		int []freq=new int[ch.length];
		
		
		for(int i=0; i<ch.length; i++)
		{
			int count=1;
			for(int j=i+1; j<ch.length; j++)
			{
				if(ch[i]==ch[j])
				{
					count++;
					freq[j]=-1;
				}
			}
			if(freq[i]!=-1)
			{
				freq[i]=count;
			}
		}
		
		for(int k=0; k<ch.length; k++)
		{
			if(freq[k]>0) {
				System.out.println(ch[k]+" "+"occurs "+freq[k]+" times");
			}
			
		}
		
	
			
	
		}

}
